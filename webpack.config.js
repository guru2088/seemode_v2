
var path = require('path');
var webpack = require('webpack');
var vtkRules = require('vtk.js/Utilities/config/dependency.js').webpack.v2.rules;

var entry = path.join(__dirname, './src/index.js');
const sourcePath = path.join(__dirname, './src/task2/');
const outputPath = path.join(__dirname, './dist/output');

module.exports = {
  entry: {
    task1:'./src/task1/index.js',
    task2:'./src/task2/index.js',
    task3:'./src/task3/index.js',
    task5:'./src/task5/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist/output'),
    filename: '[name].js',
  },
  module: {
    rules: [
        { test: /\.html$/, loader: 'html-loader' },
        { test: /\.vue/, loader: 'vue' },
        {
          test: /\.mcss$/,
          use: [
            { loader: 'style-loader' },
            {
              loader: 'css-loader',
              options: {
                localIdentName: '[name]-[local]_[sha512:hash:base32:5]',
                modules: true,
              },
            },
            // {
            //   loader: 'postcss-loader',
            //   options: {
            //     plugins: () => [autoprefixer('last 2 version', 'ie >= 10')],
            //   },
            // },
          ],
        },
        { test: /\.svg$/, loader: 'svg-loader'},
        { test: /\.(jpe?g|png|gif|svg)$/i, use: ['url-loader?limit=10000','img-loader']},
        { test: entry, loader: "expose-loader?vtk" },
    ].concat(vtkRules),
  },
  resolve: {
    modules: [
      path.resolve(__dirname, 'node_modules'),
      sourcePath,
    ],
  },
};
