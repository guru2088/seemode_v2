#!flask/bin/python
from flask import Flask
from flask import request

app = Flask(__name__)
app = Flask(__name__, static_url_path='/static')

# @app.route('/')
# def index():
#     return "Testing Rest Service"

@app.route('/seg')
def seg():
    isovalue = request.args.get('isovalue')
    filepath = request.args.get('filepath')
    if isovalue is not None and filepath is not None:
        from src.seg import segment
        result = segment(isovalue,filepath)
        return result
    else:
        return "Missing Parameters"

@app.route('/segBased')
def segBased():
    ccaSeed1_location = request.args.get('ccaSeed1_location')
    ccaSeed2_location = request.args.get('ccaSeed2_location')
    ccaSeed1_intensity = request.args.get('ccaSeed1_intensity')
    ccaSeed2_intensity = request.args.get('ccaSeed2_intensity')
    if ccaSeed1_location is not None and ccaSeed2_location is not None and ccaSeed1_intensity is not None and ccaSeed2_intensity is not None:
        from src.seedBasedSeg import segment
        result = segment(ccaSeed1_location,ccaSeed2_location,ccaSeed1_intensity,ccaSeed2_intensity)
        return result
    else:
        return "Missing Parameters"

if __name__ == '__main__':
    app.run(debug=True)
